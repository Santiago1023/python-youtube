# creo una lista
l = [2, "tres", True, ["uno", 10]]
print(l)
# accedo a un elemento
l2 = l[1]
print(l2)
# accedo a una lista dentro de una lista
l3 = l[3][0]
print(l3)
# copio de l desde la posicion 0, tres elementos
l4 = l[0:3]
print(l4)
#si quiero hacer saltos, si quiero que salte de 1 en 1 pongo 2, si quiero que salte de 2 en 2 pongo 3
l5 = l[0:3:2]
print(l5)
# todos los elementos
l6 = l[0::]
print(l6)